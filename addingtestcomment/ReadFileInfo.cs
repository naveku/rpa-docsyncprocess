﻿using DocSync.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace DocSync
{
    class ReadFileInfo
    {
        string getFileInfoRequest = @"https://www.myedoo.com/Test/Cust/api/getdocsyncfiledetails/?companyId=190&customerid=1089232&appointmentid=56789";

        public FileInfo GetFileInfo()
        {
            FileInfo result = new FileInfo();
            var client = new HttpClient();
            var response = client.GetAsync(getFileInfoRequest).Result;
            if (response.IsSuccessStatusCode)
            {
                var content = response.Content.ReadAsStringAsync();
                result = JsonConvert.DeserializeObject<FileInfo>(content.Result);
            }
            return result;
        }
    }
}

