﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Configuration;
using System.Diagnostics;
using System.Globalization;

namespace addingtestcomment
{
    public class Logger
    {
        public String logFilePath = @"c:\DocSync".ToString();

        public void log()
        {
            try
            {

                var logFilePath = @"c:\DocSync".ToString();
                if (!Directory.Exists(logFilePath))
                {
                    Directory.CreateDirectory(logFilePath);
                }

                logFilePath =
                    $@"{logFilePath}\DocSyncLog_{DateTime.Now:dd-MM-yyyy_HH_mm_ss}.txt";
            }
            catch (Exception ex)
            {
                Console.WriteLine("System Io Exception" + ex);
            }



        }
        public void WriteLog(string logMessage)
        {
            using (var w = File.AppendText(logFilePath))
            {
                //w.WriteLine("Log Entry : ");
                w.WriteLine($"{DateTime.Now.ToString(CultureInfo.InvariantCulture)} : {logMessage}");
                w.WriteLine();
            }
        }
        public void WriteLog(StreamReader r)
        {
            string line;
            while ((line = r.ReadLine()) != null)
            {
                Console.WriteLine(line);
            }
        }

    }
}
