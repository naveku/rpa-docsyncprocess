﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocSync.Model
{
    public class LoginInfo
    {
        public string OfficeNum { get; set; }
        public string OfficePassword { get; set; }
        public string AssociateName { get; set; }
        public string Associatepassword { get; set; }
    }
}