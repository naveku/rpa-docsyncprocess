﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocSync.Model
{
    public class FileInfo
    {
        public string DocumentName { get; set; }
        public string DocumentContentType { get; set; }
        public string DocumentPath { get; set; }
        public string DocumentType { get; set; }
    }
}