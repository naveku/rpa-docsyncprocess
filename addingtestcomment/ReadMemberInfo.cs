﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using DocSync.Model;


namespace DocSync
{
    class ReadMemberInfo
    {
        string getMemberInfoRequest = @"https://www.myedoo.com/Test/liveapi/api/GetCustomerInformation/?companyid=oot&patientid=1089232";

        public List<MemberInfo> MemberInfo()
        {
            List<MemberInfo> result = new List<MemberInfo>();
            var client = new HttpClient();
            var response = client.GetAsync(getMemberInfoRequest).Result;
            if (response.IsSuccessStatusCode)
            {
                var content = response.Content.ReadAsStringAsync();
                result = JsonConvert.DeserializeObject<List<MemberInfo>>(content.Result);
            }
            return result;
        }
    }
}
