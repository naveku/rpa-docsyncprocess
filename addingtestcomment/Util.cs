﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support;
using System.Diagnostics;
using OpenQA.Selenium;

namespace DocSync
{
    public class Util
    {
        public static IWebElement ExplicitWaitById(IWebDriver driver,string IdLocator)
        {
            try
            {
                WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(15));
                wait.PollingInterval = TimeSpan.FromMilliseconds(500);
                wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
                IWebElement searchElement = wait.Until(ExpectedConditions.ElementExists(By.Id(IdLocator)));
                return searchElement;
            }
            catch(Exception e)
            {
                Debug.WriteLine("Some Error:" + e.Message);
                return null;
            }
        }
        public static IWebElement ExplicitWaitByName(IWebDriver driver, string NameLocator)
        {
            try
            {
                WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(15));
                wait.PollingInterval = TimeSpan.FromMilliseconds(500);
                wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
                IWebElement searchElement = wait.Until(ExpectedConditions.ElementExists(By.Name(NameLocator)));
                return searchElement;
            }
            catch (Exception e)
            {
                Debug.WriteLine("Some Error:" + e.Message);
                return null;
            }
        }
        public static IWebElement ExplicitWaitByXpath(IWebDriver driver, string XpathLocator)
        {
            try
            {
                WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(15));
                wait.PollingInterval = TimeSpan.FromMilliseconds(500);
                wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
                IWebElement searchElement = wait.Until(ExpectedConditions.ElementExists(By.XPath(XpathLocator)));
                return searchElement;
            }
            catch (Exception e)
            {
                Debug.WriteLine("Some Error:" + e.Message);
                return null;
            }
        }

        public static IWebElement ExplicitWaitByLinkText(IWebDriver driver, string LinkTextLocator)
        {
            try
            {
                WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(15));
                wait.PollingInterval = TimeSpan.FromMilliseconds(500);
                wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
                IWebElement searchElement = wait.Until(ExpectedConditions.ElementExists(By.LinkText(LinkTextLocator)));
                return searchElement;
            }
            catch (Exception e)
            {
                Debug.WriteLine("Some Error:" + e.Message);
                return null;
            }
        }
    }

}
