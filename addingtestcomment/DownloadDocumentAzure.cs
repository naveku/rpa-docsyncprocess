﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace DocSync
{
    public class DownloadDocumentAzure
    {
        //private string getDownloadAzureFile;
        private string downloadPath;
        public string DocumentName { get; private set; }
        public DownloadDocumentAzure()
        {
            //getDownloadAzureFile = @"https://www.myedoo.com/Test/Cust/api/document/?appointmentid=123";
            downloadPath = ConfigurationManager.AppSettings["DownloadDirectory"];
        }

        public void Download()
        {
            var readFileInfo = new ReadFileInfo();
            var fileInfo = readFileInfo.GetFileInfo();
            HttpWebResponse webResponse = GetWebResponse(fileInfo.DocumentPath);
            string filebytes = string.Empty;
            //using (var v = new StreamReader(webResponse.GetResponseStream()))
            //{
            //    filebytes = v.ReadToEnd().Replace("�","");
            //}
            var fileByte = JsonConvert.DeserializeObject<byte[]>(filebytes);
            //Invoke api 
            try
            {
                DocumentName = Path.Combine(downloadPath, fileInfo.DocumentName);
                using (var client = new WebClient())
                {
                    switch (fileInfo.DocumentContentType.ToLowerInvariant())
                    {
                        case "jpg":
                            client.DownloadFile(new Uri(fileInfo.DocumentPath), DocumentName);
                            break;
                        case "pdf":
                            client.DownloadFile(new Uri(fileInfo.DocumentPath), DocumentName);
                            break;
                        case "png":
                            client.DownloadFile(new Uri(fileInfo.DocumentPath), DocumentName);
                            break;
                        case "gif":
                            client.DownloadFile(new Uri(fileInfo.DocumentPath), DocumentName);
                            break;
                        default:
                            break;
                    }
                }
            } catch(Exception ex)
            {
                Console.WriteLine(string.Format($"Error occurred: {0}", ex.Message));
            }
        }

        public HttpWebResponse GetWebResponse(string request)
        {
            try
            {
                ////Get the saved Access Token from Session
                //accessToken = (string)HttpContext.Current.Session["AccessToken"];

                HttpWebRequest webrequest = (HttpWebRequest)WebRequest.Create(request);
                webrequest.KeepAlive = true;
                webrequest.Method = "GET";

                //Append The Access Tokken with the Web request
                //webrequest.PreAuthenticate = true;
                //webrequest.Headers.Add("Authorization", "Bearer " + accessToken);
                ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

                HttpWebResponse webresponse = (HttpWebResponse)webrequest.GetResponse();
                return webresponse;
            }
            catch (Exception ex)
            {
                //_miscellaneousServices.LogToDb(ex.Message, "error");
                return null;
            }
        }
    }

   
}