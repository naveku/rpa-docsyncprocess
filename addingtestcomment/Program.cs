﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions.Internal;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support;
using System.Data;
using DocSync.Model;
using System.IO;
using System.Net.Mail;
using System.Threading;
using OpenQA.Selenium.Interactions;

namespace DocSync
{

    class Program
    {
        private static ChromeOptions Options;
        private static IWebDriver driver;
        private static DownloadDocumentAzure downloadDocumentFromAzure;
        //IWebDriver driver = new ChromeDriver();


        public static void Main(string[] args)
        {
            downloadDocumentFromAzure = new DownloadDocumentAzure();
            downloadDocumentFromAzure.Download();
            //Options = new ChromeOptions();
            //Options.AddArgument("--headless");
            //driver =  new ChromeDriver(Options);
             
            OpenSite();
            ScrappingAction();
        }

        public static void OpenSite()
        {
            try
            {
                //ChromeOptions Options = new ChromeOptions();
                //Options.AddArgument("--headless");
                //IWebDriver driver = new ChromeDriver(Options);
               driver.Navigate().GoToUrl("https://www.myedoo.com/blink");
                driver.Manage().Window.Maximize();
                
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        public static void ScrappingAction()
        {
            var LoginInfo = new ReadLoginApidata().GetLoginInfo();
            foreach(var login in LoginInfo)
            {
                Util.ExplicitWaitById(driver, "OfficeTextBox").SendKeys(login.OfficeNum);
                Util.ExplicitWaitById(driver, "PasswordTextBoxPOS").SendKeys(login.OfficePassword);
                Util.ExplicitWaitById(driver, "OkButton").Click();
                Util.ExplicitWaitById(driver, "LoginControl_rcbUserName_Input").SendKeys(login.AssociateName);
                Util.ExplicitWaitById(driver, "LoginControl_Password").SendKeys(login.Associatepassword);
                Util.ExplicitWaitById(driver, "LoginControl_Authenticate").Click();


                var FileInfo = new ReadFileInfo().GetFileInfo();

                var Memberinfo = new ReadMemberInfo().MemberInfo();
                foreach (var Member in Memberinfo)
                {
                    var date = Convert.ToDateTime(Member.DateOfBirth).ToString("MM/dd/yyyy");
                    Util.ExplicitWaitById(driver, "ctl00_ctl00_MainContent_MainContent_PatientSearch_LastName").SendKeys(Member.LastName);
                    Util.ExplicitWaitById(driver, "ctl00_ctl00_MainContent_MainContent_PatientSearch_FirstName").SendKeys(Member.FirstName);
                    Util.ExplicitWaitById(driver, "ctl00_ctl00_MainContent_MainContent_PatientSearch_DateOfBirth_dateInput").SendKeys(date);
                    Util.ExplicitWaitById(driver, "ctl00_ctl00_MainContent_MainContent_PatientSearch_PhoneNumber").SendKeys(Member.PhoneNumber);
                    
                    
                    Util.ExplicitWaitById(driver, "ctl00_ctl00_MainContent_MainContent_PatientSearch_PatientResultGrid_ctl00_ctl04_LastNameText").Click();
                    Util.ExplicitWaitById(driver, "ctl00_ctl00_MainContent_MainContent_PatientEditControl_idDocument_SubmitButton").Click();

                    driver.Navigate().GoToUrl("https://www.myedoo.com/Blink/Dialogs/Patient/DisplayDocumentDialog.aspx");
                    

                    Util.ExplicitWaitByName(driver, "ctl00$MainContent$PatientDocumentControl$btnbrowse").Click();



                    Util.ExplicitWaitById(driver, "ctl00_MainContent_PatientDocumentControl_cmbType_Arrow").Click();

                    var drpvalue = driver.FindElement(By.Id("ctl00_MainContent_PatientDocumentControl_cmbType_DropDown")).Text;
                    String[] breakApart = drpvalue.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
                                         
                       if(FileInfo.DocumentType.ToLowerInvariant() == ("Insurance Card").ToLowerInvariant())
                        {
                          driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Driving License'])[1]/following::li[1]")).Click();

                        }

                        if (FileInfo.DocumentType.ToLowerInvariant() == ("Medical History").ToLowerInvariant())
                        {

                        driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Insurance Card'])[1]/following::li[1]")).Click();

                        }

                     if (FileInfo.DocumentType.ToLowerInvariant() == ("Other Document").ToLowerInvariant())
                     {

                        driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Other Document'])[1]/following::li[1]")).Click();

                     }






                    Util.ExplicitWaitById(driver, "ctl00_MainContent_PatientDocumentControl_RadName").SendKeys(FileInfo.DocumentName);

                    OpenQA.Selenium.Interactions.Actions actions = new Actions(driver);
                    
                    IWebElement elementLocator = driver.FindElement(By.Id("ctl00_MainContent_PatientDocumentControl_RadAsyncUpload1file0"));
                    actions.ClickAndHold(elementLocator).Perform();

                    if (File.Exists(downloadDocumentFromAzure.DocumentName))
                    {
                        elementLocator.SendKeys(downloadDocumentFromAzure.DocumentName);
                        Util.ExplicitWaitById(driver, "ctl00_MainContent_PatientDocumentControl_btnSave").Click();
                        Util.ExplicitWaitById(driver, "ctl00_MainContent_PatientDocumentControl_CancelButton").Click();
                    } else
                    {
                        //TODO: Log message that document not found the given location
                        Console.WriteLine($"Document not found: {0}", downloadDocumentFromAzure.DocumentName);
                    }

                    driver.Navigate().GoToUrl("https://www.myedoo.com/Blink/SelectPatient.aspx?fromPatBtn=1");


                }

                Util.ExplicitWaitById(driver, "ctl00_ctl00_BtnLogOut").Click();

                
            }

            driver.Close();
        }
        

    }


}
